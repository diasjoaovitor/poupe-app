import { Box, Button, Divider, FormControl } from "@mui/material"
import { AttachMoney } from '@mui/icons-material'
import { Header, Note, Type } from "../../shared/components"
import { usePost } from "../../shared/hooks"
import * as S from './style'

export const Post: React.FC = () => {
  const {
    title, color, 
    handleTypeChange,
    borderColor, transaction,
    handleDateChange, handleSubmit
  } = usePost()

  return (
    <Box sx={S.post}>
      <Header title={title} comeBack={true} color={color}>
        <AttachMoney color={color} fontSize="large" />
      </Header>
      <Divider />
      <Box 
        component="form" borderColor={`${borderColor} !important`}
        onSubmit={handleSubmit}
      >
        <Type handleTypeChange={handleTypeChange} />
        <Divider />
        <Note 
          transaction={transaction} color={color} 
          handleDateChange={handleDateChange} />
        <Divider />
        <FormControl fullWidth>
          <Button type="submit" variant="contained" color={color}>
            Salvar
          </Button>
        </FormControl>
      </Box>
    </Box>
  )
}
