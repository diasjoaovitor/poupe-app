import { createContext, ReactNode, useContext, useState } from "react"
import { TTransaction } from "../types"
import { transaction as t } from '../states'

type TTransactionContext = {
  transactionContext: TTransaction
  setTransactionContext(transaction: TTransaction): void
}

const TransactionContext = createContext({} as TTransactionContext)

export const useTransactionContext = () => useContext(TransactionContext)

export const TransactionProvider: React.FC<{children: ReactNode}> = ({ children }) => {
  const [ transaction, setTransaction ] = useState(t)

  return (
    <TransactionContext.Provider 
      value={{ 
        transactionContext: transaction, 
        setTransactionContext: setTransaction 
      }}
    >
     {children}
    </TransactionContext.Provider>
  )
}
