import { useState } from "react"
import { Box, IconButton, Menu as MatMenu, MenuItem } from "@mui/material"
import { Logout, Menu as MenuIcon } from "@mui/icons-material"
import { logout } from "../../firebase"
import * as S from './style'

export const Menu: React.FC = () => {
  const [ anchorEl, setAnchorEl ] = useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    setAnchorEl(event.currentTarget)
  }
   
  const handleClose = (): void => {
    setAnchorEl(null)
  }

  return (
    <Box component="span">
      <IconButton onClick={handleClick}>
        <MenuIcon />
      </IconButton>
      <MatMenu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        sx={S.menu}
      >
          <MenuItem onClick={logout}>
            <Logout /> Logout
          </MenuItem>
      </MatMenu>
    </Box>
  )
}