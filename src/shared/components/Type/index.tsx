import { FormControl, FormControlLabel, Radio, RadioGroup, Typography } from "@mui/material"
import { ChangeEvent } from "react"
import * as S from './style'

type Props = {
  handleTypeChange(e: ChangeEvent<HTMLInputElement>): void
}

export const Type: React.FC<Props> = ({ handleTypeChange }) => {
  return (
    <FormControl sx={S.transactionType}>
      <Typography component="label">Tipo de lançamento</Typography>
      <RadioGroup defaultValue="Despesa">
        <FormControlLabel 
          label="Despesa" value="Despesa" name="type"
          control={<Radio color="error" onChange={handleTypeChange} />}  
        />
        <FormControlLabel 
          label="Receita" value="Receita" name="type"
          control={<Radio onChange={handleTypeChange} />} 
        />
      </RadioGroup>
    </FormControl>
  )
}