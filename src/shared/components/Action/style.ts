import { SxProps, Theme } from "@mui/material"

export const action: SxProps<Theme> = {
  '& .MuiDialogContent-root': {
    paddingBottom: 0
  },
  '& .css-nen11g-MuiStack-root': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    cursor: 'pointer'
  },
  '& .css-nen11g-MuiStack-root:last-child': {
    marginTop: 2
  },
  '& .MuiDialogActions-root': {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    margin: 2
  }
}
