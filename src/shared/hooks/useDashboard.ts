import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { SelectChangeEvent } from "@mui/material"
import { transaction as t, wallet as w  } from "../states"
import { TTransaction } from "../types"
import { destroy, read } from "../firebase"
import { useAuthContext, useTransactionContext } from "../contexts"

export const useDashboard = () => {
  const { user } = useAuthContext()
  const { setTransactionContext } = useTransactionContext()
  const navigate = useNavigate()
  const [ month, setMonth ] = useState('Setembro')
  const [ year, setYear ] = useState<string | number>(2022)
  const [ transaction, setTransaction ] = useState<TTransaction>()
  const [ transactions, setTransactions ] = useState<TTransaction[]>([])
  const [ wallet, setWallet ] = useState(w)

  useEffect(() => {
    (async () => {
      const period = `${month}/${year}`
      const transactions: TTransaction[] = await read(user?.uid as string, period)
      const expenses = transactions.filter(({ type }) => type === 'Despesa')
      const incomes = transactions.filter(({ type }) => type === 'Receita')
      const totalExpenses = expenses.reduce((count, { value }) => count += value, 0)
      const totalIncomes = incomes.reduce((count, { value }) => count += value, 0)
      const wallet = {
        expenses: totalExpenses,
        incomes: totalIncomes,
        balance: totalIncomes - totalExpenses
      }
      setWallet(wallet)
      setTransactions(transactions)
      setTransactionContext(t)
    })()
  }, [year, month])

  const handleMonthChange= (e: SelectChangeEvent) => {
    setMonth(e.target.value)
  }

  const handleYearChange= (e: SelectChangeEvent) => {
    setYear(e.target.value)
  }

  const handleActionClick = (transaction: TTransaction) => {
    setTransaction(transaction)
  }

  const handleClose = () => {
    setTransaction(undefined)
  }

  const handleUpdate = () => {
    setTransactionContext(transaction as TTransaction)
    navigate('/post/editar')
  }

  const handleDelete = async () => {
    transaction && await destroy(transaction.id as string)
    setTransaction(undefined)
  }
  
  return {
    month, handleMonthChange,
    year, handleYearChange,
    wallet, transactions,
    transaction, handleActionClick,
    handleClose, handleUpdate, handleDelete
  }
}
