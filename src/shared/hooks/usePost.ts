import { ChangeEvent, FormEvent, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import { Dayjs } from "dayjs"
import { useAuthContext, useTransactionContext } from "../contexts"
import { create, update } from "../firebase"
import { months } from "../states"
import { TTransactionType, TTransaction, TTypeColor } from "../types"
import { getColor, getElementValues } from "../functions"

export const usePost = () => {
  const navigate = useNavigate()
  const { pathname } = useLocation()
  const { user } = useAuthContext()
  const { transactionContext } = useTransactionContext()
  
  const [ color, setColor ] = useState<TTypeColor>(getColor(transactionContext.type))
  const [ transaction, setTransaction ] = useState<TTransaction>(transactionContext)

  const title = pathname === '/post/adicionar' ? 'Adicionar' : 'Editar'
  const borderColor = color === 'error' ? '#f44336' : '#90caf9'

  const handleTypeChange = (e: ChangeEvent<HTMLInputElement>) => {
    setColor(getColor(e.target.value as TTransactionType))
    setTransaction({ ...transaction, type: e.target.value as TTransactionType })
  }

  const handleDateChange = (e: Dayjs) => {
    const date = e.format('YYYY/MM/DD')
    setTransaction({ ...transaction, date })
  }

  const handleSubmit = async (e: FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault()
    try {
      const [ description, value ] = getElementValues(e, ['description', 'value'])
      const [ year, month ] = transaction.date.split('/')
      const t: TTransaction = {
        ...transaction,
        description,
        value: Number(value),
        period: `${months[Number(month) - 1]}/${year}`,
        ref: user?.uid as string,
        timestamp: (new Date()).toLocaleString("en", { dateStyle: "medium", timeStyle: "medium" })
     }
     console.log('carregando...')
      pathname === '/post/adicionar' ? 
        await create(t) : await update(t)
     console.log('finish')
     navigate('/')
    } catch (error) {
      alert(error)
    }
  }

  return {
    title, color, 
    handleTypeChange,
    borderColor, transaction, 
    handleDateChange, handleSubmit
  }
}
