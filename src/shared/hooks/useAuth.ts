import { FormEvent } from "react"
import { useNavigate } from "react-router-dom"
import { getElementValues } from "../functions"
import { TAuthService } from "../types"

export const useAuth= (auth: TAuthService) => {
  const navigate = useNavigate()

  const handleSubmit = async (e: FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault()
    try {
      const [ email, password, passwordConfirm ] = getElementValues(e, [ 'email', 'password', 'passwordConfirm' ])
      if(passwordConfirm && (password !== passwordConfirm)) {
        alert('As senhas são diferentes')
        return
      }
      await auth(email, password)
      navigate('/')
    } catch (error) {
      alert(error)
    }
  }

  return {
    handleSubmit
  }
}
